const HTTP = require('http');
const PORT = 4000
HTTP.createServer((request,response) => {
    if(request.url == `/profile` && request.method == `GET`){
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Welcome to your profile")
    } else if (request.url == `/courses` && request.method == `GET`){
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Here's our courses available")
    } else if (request.url == `/addcourse` && request.method == `POST`){
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Add course to our resources")
    }else if (request.url == `/updatecourse` && request.method == `PUT`){
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Update a course to our resources")
    }else if (request.url == `/archivecourse` && request.method == `DELETE`){
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Archive courses to our resources")
    }else {
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Welcome to booking system")
    }

}).listen(PORT,()=>{console.log(`Server is running on port ${PORT}`)})